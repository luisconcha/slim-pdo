<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Dashboard FTTH</title>
    <link rel="stylesheet" href="../public/assets/bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../public/css/maps.css"/>

</head>
<body>
<div id="initial-loader">
    <span id="initial-loader-msg">Construindo análise do Mapa. Por favor aguarde...</span>
</div>


<div class="container">
    <div class="row" id="header">
        <div class="col-md-3"><img src="../public/img/oi.png" alt="" id="logo-oi"></div>
        <div class="col-md-6">
            <h3>Dashboard FTTH - SOC</h3>
            <h6>Chamadas consultadas no REC</h6>
        </div>
        <div class="col-md-3"><img src="../public/img/global.png" alt="" id="global"></div>
    </div>


    <!--

     id = document.getElementById('svg-map').children[0].children[0].id
     tam = document.getElementById('svg-map').children[0]
     tam.childElementCount

    -->

    <div class="row">
        <div class="col-md-5">
            <svg version="1.1" id="svg-map" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink"
                 x="0px" y="0px" width="450px" height="460px" viewBox="0 0 450 460"
                 enable-background="new 0 0 450 460"
                 xml:space="preserve">
                <g>
                    <?php

                    foreach ( $mapsJson[ 'maps' ] as $k => $v ):
                        ?>
                        <a xlink:href="<?php echo $v[ 'href' ] ?>"
                           class="states"
                           id="<?php echo $v[ 'id' ] ?>"
                           data-id="<?php echo $v[ 'data_id' ] ?>"
                           data-key="<?php echo $v[ 'data_key' ] ?>"
                           onclick="listDataByState('<?php echo $v[ 'data_key' ] ?>')">
                            <path stroke="#FFFFFF" stroke-width="1.0404" stroke-linecap="round" stroke-linejoin="round"
                                  d="<?php echo $v[ 'd' ] ?>"></path>

                            <?php if ( isset( $v[ 'd1' ] ) ) : ?>
                                <path class="circle" d="<?php echo $v[ 'd1' ] ?>"></path>
                            <?php endif; ?>

                            <text transform="<?php echo $v[ 'transform' ] ?>"
                                  fill="#FFFFFF"><?php echo $v[ 'state_acronym' ] ?></text>
                        </a>
                    <?php
                    endforeach;
                    ?>
                </g>
            </svg>
        </div>
        <div class="col-md-7" id="top-10">

            <div class="container-accordion">
                <div class="clearfix"></div>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                   aria-expanded="true" aria-controls="collapseOne">
                                    <span class="label label-default">Top 10 GALC</span>
                                </a>
                            </h4>
                        </div>
                       
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                             aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div id="top-10">
                                    <div id="alarms"></div>
                                    <div id="mensagem"></div>
                                    <table class="table table-striped" width='100%' id="tb_olt_top">
                                        <thead>
                                        <tr class="tr-title">
                                            <td>GALC(OLT)</td>
                                            <td class="mediaDetachment"></td>
                                            <td class="average"></td>
                                            <td class="min10"></td>
                                            <td class="days7"> </td>
                                            <td class="days14"></td>
                                            <td class="days21"></td>
                                            <td class="days28"></td>
                                            <td class="days35"></td>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default" id="div-galc-by-state">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <span class="label label-default">Galc da UF: </span> &nbsp;
                                    <span id="state-galc" class="label label-success"></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel"
                             aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <div id="msg-galc-by-state"></div>
                                <div id="tblGalcUF">
                                    <table class="table table-striped" width='100%' id="tb_galc_by_state">
                                        <thead>
                                        <tr class="tr-title">
                                            <td>Galc(OLT)</td>
                                            <td class="mediaDetachment"></td>
                                            <td class="average"></td>
                                            <td class="min10"></td>
                                            <td class="days7"> </td>
                                            <td class="days14"></td>
                                            <td class="days21"></td>
                                            <td class="days28"></td>
                                            <td class="days35"></td>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div> <!-- /container -->


<!-- Modal -->
<div class="modal fade" id="modalReporter">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                
                <ul class="list-group">
                    <li class="list-group-item">Alarme presente no sicaweb (acesso)
                        <span class="badge label-primary" id="alarme_sicaweb"></span>
                    </li>
                    <li class="list-group-item">Quantidade de alarmes nos últimos 60 minutos<span class="badge label label-default last_60min"></span></li>
                    <li class="list-group-item">Quantidade de alarmes no dia<span class="badge label label-default no_dia"></span></li>
                    <li class="list-group-item">Comunicado de interrupção<span class="badge label label-default comunicacao"></span></li>
                    <li class="list-group-item">Vocalização no REC<span class="badge label label-default vocalizacao"></span></li>
                    <li class="list-group-item">BA<span class="badge label label-default ba_galc"></span></li>
<!--                    <li class="list-group-item">JM em execução<span class="badge">SIM</span></li>-->
<!--                    <li class="list-group-item">BD aberto<span class="badge">SIM</span></li>-->
                    <li class="list-group-item">Média histórica de ligações
                        <span class="badge" id="value-average"></span>
                    </li>
                    <li class="list-group-item">Volume de ligações por porta pon
                        <span class="badge" id="spanListaPon"></span>
                    </li>
                </ul>

                <div id="divListGalcGMON">
                    <span class="badge badge-info" id="msg-galc-gmon"></span>
                    <div id="listaGalcGMON"></div>
                </div>

                <div id="listPortaPont">
                    <span class="badge badge-info" id="msg-porta-pont"></span>
                    <div id="listaDasPortasPont"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-fechar-modal" data-dismiss="modal"
                ">Fechar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Detalhamento Galc por dia -->
<div class="modal fade" id="modalDetailGalcByDay">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detalhamento do Galc</h4>
            </div>
            <div class="modal-body">

                <div id="divDetailGalcByDay">
                    <span class="badge badge-info" id="msg-detail-galc-by-day"></span>
                </div>
                <table class="table table-striped" width='100%'>
                    <thead>
                    <tr  class="tr-title">
                        <td>Galc(OLT)</td>
                        <td>UF</td>
                        <td>Localidade</td>
                        <td>Município</td>
                        <td>Porta PON</td>
                        <td>BA</td>
                        <td>Tipo de Evento</td>
                        <td>QTD</td>
                    </tr>
                    </thead>
                    <tbody id="tbody_detail-galc-by-day">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-fechar-modal" data-dismiss="modal"
                ">Fechar</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal Galc by State-->
<div class="modal fade" id="modalPortaPontByState">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-uf-title"></h4>
            </div>
            <div class="modal-body">

                <ul class="list-group">
                    <li class="list-group-item">Alarme presente no sicaweb (acesso)
                        <span class="badge label-primary" id="alarme_sicaweb_df"></span>
                    </li>
                    <li class="list-group-item">Quantidade de alarmes nos últimos 60 minutos<span class="badge label label-default last_60min_uf"></span></li>
                    <li class="list-group-item">Quantidade de alarmes no dia<span class="badge label label-default no_dia_uf"></span></li>
                    <li class="list-group-item">Comunicado de interrupção<span class="badge label label-default comunicacao_uf"></span></li>
                    <li class="list-group-item">Vocalização no REC<span class="badge label label-default vocalizacao_uf"></span></li>
                    <li class="list-group-item">BA<span class="badge label label-default ba_galc_uf"></span></li>
<!--                    <li class="list-group-item">JM em execução<span class="badge">SIM</span></li>-->
<!--                    <li class="list-group-item">BD aberto<span class="badge">SIM</span></li>-->
                    <li class="list-group-item">Média histórica de ligações
                        <span class="badge" id="value-df-average"></span>
                    </li>
                    <li class="list-group-item">Volume de ligações por porta pon
                        <span class="badge" id="spanListaPonByUf"></span>
                    </li>
                </ul>

                <div id="divListGalcGMONState">
                    <span class="badge badge-info" id="msg-galc-gmon"></span>
                    <div id="listaGalcGMONState"></div>
                </div>

                <div id="listPortaPontByState">
                    <span class="badge badge-info" id="msg-porta-pont-by-state"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-fechar-modal" data-dismiss="modal"
                ">Fechar</button>
            </div>
        </div>
    </div>
</div>


<footer class="fixed-bottom" id="footer">
    <p>Dashboard atualizado em: <span id="now"></span> &nbsp; Próxima atualização: <span id="next-update"></span></p>
</footer>
</body>

<script src="../public/js/jquery.js"></script>
<script src="../public/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../public/assets/bootbox/bootbox.js"></script>
<script src="../public/js/moment.js"></script>
<script src="../public/js/moment-with-locales.min.js"></script>
<script src="../public/js/home.js"></script>
<script src="../public/js/maps.js"></script>
<script src="../public/js/reportes_status.js"></script>

<script type="text/javascript">
    $( document ).ready( function () {

        setTimeout(function() {
            console.log('Recarregando análise....: ');
            
            location.reload();
        }, 600000); //10minutos
    } )
</script>
</html>
