$( document ).ready( function () {

    const now       = moment();
    let las10minute = moment();
    las10minute     = las10minute.add( 10, 'minutes' );
    
    $( '.container' ).hide();
    $( '#footer' ).hide();

    $( window ).load( function () {
        
        $( "#initial-loader" ).delay( 6000 ).fadeOut( "slow", function ( e ) {
            $( '.container' ).show();
            $('#footer').show();

            $('#now').html(now.format( 'DD/MM/YYYY HH:mm' ));
            $('#next-update').html(las10minute.format( 'DD/MM/YYYY HH:mm' ));
        } );

    } );

    $( ".toggle-accordion" ).on( "click", function () {
        var accordionId  = $( this ).attr( "accordion-id" ),
            numPanelOpen = $( accordionId + ' .collapse.in' ).length;

        $( this ).toggleClass( "active" );

        if ( numPanelOpen == 0 ) {
            openAllPanels( accordionId );
        } else {
            closeAllPanels( accordionId );
        }
    } );

    openAllPanels  = function ( aId ) {
        console.log( "setAllPanelOpen" );
        $( aId + ' .panel-collapse:not(".in")' ).collapse( 'show' );
    };
    closeAllPanels = function ( aId ) {
        console.log( "setAllPanelclose" );
        $( aId + ' .panel-collapse.in' ).collapse( 'hide' );
    };

} );
