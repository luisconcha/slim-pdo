$( document ).ready( function () {

    let tb_olt              = $( '#tb_olt_top' );
    let tb_reporter_status  = $( '#tb_reporter_status' );
    let tb_galc_by_state    = $( '#tb_galc_by_state' );
    let tbody               = tb_olt.find( 'tbody' );
    let tbodyReporterStatus = tb_reporter_status.find( 'tbody' );
    let tbodyTbGalcByState  = tb_galc_by_state.find( 'tbody' );
    let htmlTop10           = '';
    let htmlGalcByState     = '';
    let state               = '';
    let stateKey            = '';
    let divGalcByState      = $( '#div-galc-by-state' );

    let loading = $( '#loading' );

    loading.hide();
    divGalcByState.hide();

    /**
     * Verifica as cores do mapa para mostrar os alarms
     */
    $.ajax( {
        url: '/average-check',
        type: 'POST',
        beforeSend: function () {
            loading.show();
        },
        success: function ( data ) {

            let $arrCores = [];
            //Cores do Mapa
            $.each( data[ 'resultAverage' ], function ( index, value ) {
                $.each( value, function ( idx, vl ) {
                    switch ( vl.UF ) {
                        case 'AC':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'AL':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'AM':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'AP':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'BA':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'CE':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'DF':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'ES':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'GO':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'MA':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'MT':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'MS':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'MG':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'PA':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'PB':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'PR':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'PE':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'PI':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'RJ':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'RN':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'RS':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'RO':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'RR':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'SC':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'SP':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'SE':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                        case 'TO':
                            $arrCores.push( vl.color_alarm );
                            attachColorAlarmeInUF( vl.UF, $arrCores );
                            $arrCores = [];
                            break;
                    }

                } );

                loading.hide();
            } );

            /**
             * Lista os doas do Galc por estado (ao clicar no mapa)
             * @param idState
             */
            listDataByState = function ( idState ) {

                divGalcByState.show();

                $( '#state-galc' ).html( idState );

                let arrDataByState = [];
                $.each( data[ 'resultAverage' ], function ( idx, valueState ) {
                    $.each( valueState, function ( index, value ) {
                        if ( value.UF === idState ) {
                            arrDataByState.push( value );
                        }
                    } )
                } );

                if ( arrDataByState.length === 0 ) {

                    tbodyTbGalcByState.html( '<tr><td class="noRecord" colspan="9"><span class="badge badge-info">Não existem registros do galc para esse estado</span></td></tr>' );
                } else {

                    htmlGalcByState = '';

                    $.each( arrDataByState, function ( idx, vl ) {

                        htmlGalcByState += '<tr>';
                        htmlGalcByState += `<td><a onclick="openDetailGalcByState(this)"
                                    data-galc='${ JSON.stringify( vl ) }'
                                    class="home-top"
                                    data-uf="${ vl.UF }"
                                    data-id-galc="${ vl.GALC_OLT }">${ vl.GALC_OLT }</a>
                                    </td>`;

                        
                        let percente = (vl.media_detachment === 'OK') ? '':'%';

                        if ( vl.color_alarm === 'alarm-red' ) {
                            htmlGalcByState += `<td class="alarmeErro"> ${vl.media_detachment}${percente} </td>`;
                        } else if ( vl.color_alarm === 'alarm-yellow' ) {
                            htmlGalcByState += `<td class="alarmeAtencao"> ${vl.media_detachment}${percente} </td>`;
                        } else {
                            htmlGalcByState += `<td>${vl.media_detachment}${percente}</td>`;
                        }

                        htmlGalcByState += '<td>' + vl.media + '</td>';

                        htmlGalcByState += '<td>' + vl.TOTAL + '</td>';


                        ( vl.TOTAL_7 !== '0' ) ?
                            htmlGalcByState += `<td><a href="#"  data-days='7' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_7 }</a></td>`
                            : htmlGalcByState += `<td>${ vl.TOTAL_7 }</td>`;

                        ( vl.TOTAL_14 !== '0' ) ?
                            htmlGalcByState += `<td><a href="#"  data-days='14' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_14 }</a></td>`
                            : htmlGalcByState += `<td>${ vl.TOTAL_14 }</td>`;

                        ( vl.TOTAL_21 !== '0' ) ?
                            htmlGalcByState += `<td><a href="#"  data-days='21' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_21 }</a></td>`
                            : htmlGalcByState += `<td>${ vl.TOTAL_21 }</td>`;

                        ( vl.TOTAL_28 !== '0' ) ?
                            htmlGalcByState += `<td><a href="#"  data-days='28' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_28 }</a></td>`
                            : htmlGalcByState += `<td>${ vl.TOTAL_28 }</td>`;

                        ( vl.TOTAL_35 !== '0' ) ?
                            htmlGalcByState += `<td><a href="#"  data-days='35' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_35 }</a></td>`
                            : htmlGalcByState += `<td>${ vl.TOTAL_35 }</td>`;

                        htmlGalcByState += '</tr>';
                    } );

                    tbodyTbGalcByState.html( htmlGalcByState );
                }

            };
            console.log('TOP10: ', data[ 'top10Galc' ]);

            //Monta os top 10 GALC
            if ( data[ 'top10Galc' ].length > 0 ) {
                $.each( data[ 'top10Galc' ], function ( idx, vl ) {

                    let response = vl;

                    htmlTop10 += '<tr>';
                    htmlTop10 += `<td><a onclick="openDetailGalc(this)" 
                                    data-galc='${ JSON.stringify( vl ) }'
                                    class="home-top"  
                                    data-id-galc="${ vl.GALC_OLT }">${ vl.GALC_OLT }</a>
                                    </td>`;

                    if ( vl.color_alarm === 'alarm-red' ) {
                        htmlTop10 += '<td class="alarmeErro">' + vl.media_detachment + '%</td>';
                    } else if ( vl.color_alarm === 'alarm-yellow' ) {
                        htmlTop10 += '<td class="alarmeAtencao">' + vl.media_detachment + '%</td>';
                    } else {
                        htmlTop10 += '<td>' + vl.media_detachment + '%</td>';
                    }

                    htmlTop10 += '<td>' + vl.media + '</td>';

                    ( vl.TOTAL !== '0' ) ?
                        htmlTop10 += `<td><a href="#"  data-days='1' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL }</a></td>`
                        : htmlTop10 += `<td>${ vl.TOTAL }</td>`;

                    ( vl.TOTAL_7 !== '0' ) ?
                        htmlTop10 += `<td><a href="#"  data-days='7' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_7 }</a></td>`
                        : htmlTop10 += `<td>${ vl.TOTAL_7 }</td>`;

                    ( vl.TOTAL_14 !== '0' ) ?
                        htmlTop10 += `<td><a href="#"  data-days='14' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_14 }</a></td>`
                        : htmlTop10 += `<td>${ vl.TOTAL_14 }</td>`;

                    ( vl.TOTAL_21 !== '0' ) ?
                        htmlTop10 += `<td><a href="#"  data-days='21' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_21 }</a></td>`
                        : htmlTop10 += `<td>${ vl.TOTAL_21 }</td>`;

                    ( vl.TOTAL_28 !== '0' ) ?
                        htmlTop10 += `<td><a href="#"  data-days='28' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_28 }</a></td>`
                        : htmlTop10 += `<td>${ vl.TOTAL_28 }</td>`;

                    ( vl.TOTAL_35 !== '0' ) ?
                        htmlTop10 += `<td><a href="#"  data-days='35' data-galc='${ JSON.stringify( vl.GALC_OLT ) }' onclick="openDetailGalcBySpecificDay(this)"> ${ vl.TOTAL_35 }</a></td>`
                        : htmlTop10 += `<td>${ vl.TOTAL_35 }</td>`;

                    htmlTop10 += '</tr>';
                } );
            }else{
                htmlTop10 += '<tr>';
                htmlTop10 += '<td colspan="9"><span class="badge badge-warning no-data">Sem dados na base de alarmes FTTH</span></td>';
                htmlTop10 += '</tr>';
            }
            tbody.html( htmlTop10 );

        }//end success
    } );

    function attachColorAlarmeInUF( uf, arr_colors_alarme ) {

        let arr_uf = [ "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO" ];

        var countRed = 0;
        var isRed    = false;
        $.each( arr_uf, function ( idx, vl ) {
            if ( vl === uf ) {
                $.each( arr_colors_alarme, function ( i, v ) {
                    if ( v === 'alarm-red' ) {
                        countRed += 1;
                        isRed = true;
                        $( `#${ uf } > path` ).attr( 'class', 'alarm-red' );
                    }
                } );
            }
        } );

        $.each( arr_uf, function ( idx, vl ) {
            if ( vl === uf ) {
                $.each( arr_colors_alarme, function ( i, v ) {
                    if ( v === 'alarm-yellow' ) {
                        if ( $( `#${ uf } > path` ).attr( 'class' ) !== "alarm-red" ) {
                            $( `#${ uf } > path` ).attr( 'class', 'alarm-yellow' );
                        }
                    }
                } );
            }
        } );

    }

    /**
     * Verifica a cor padrão dos estados com classe "circle" para adicionar a cor verde-escuro caso
     * o color_alarm =  alarm-green
     * @param uf
     * @param color_alarm
     * @returns {boolean}
     */
    function attachDefaulColorInCicle( uf, color_alarm ) {
        var status = false;
        if ( color_alarm === 'alarm-green' ) {
            switch ( uf ) {
                case 'DF':
                    status = true;
                    break;
                case 'RJ':
                    status = true;
                    break;
                case 'RN':
                    status = true;
                    break;
                case 'PB':
                    status = true;
                    break;
                case 'AL':
                    status = true;
                    break;
            }
        }
        return status;
    }

} );
