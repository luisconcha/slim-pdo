$( document ).ready( function () {
    moment.locale( 'pt-BR' );

    let listPortaPont        = $( '#listPortaPont' );
    let listaDasPortasPont   = $( '#listaDasPortasPont' );
    let msgPortaPont         = $( '#msg-porta-pont' );
    let listPortaPontByState = $( '#listPortaPontByState' );
    let msgPortaPontByState  = $( '#msg-porta-pont-by-state' );
    let comunicacao          = $( '.comunicacao' );
    let vocalizacao          = $( '.vocalizacao' );
    let baGalc               = $( '.ba_galc' );
    let comunicacaoUF        = $( '.comunicacao_uf' );
    let vocalizacaoUF        = $( '.vocalizacao_uf' );
    let baGalcUf             = $( '.ba_galc_uf' );
    let listaGalcGMON        = $( '#listaGalcGMON' );
    let listaGalcGMONState   = $( '#listaGalcGMONState' );
    let last_60min           = $( '.last_60min' );
    let no_dia               = $( '.no_dia' );
    let last_60min_uf        = $( '.last_60min_uf' );
    let no_dia_uf            = $( '.no_dia_uf' );

    msgPortaPont.hide();
    msgPortaPontByState.hide();

    createHeaderAverage();

    /**
     * Mostra os detalhes do Galc na modal
     * @param data
     */

    openDetailGalc = function ( data ) {

        let data_galc = JSON.parse( $( data ).attr( 'data-galc' ) );
        let idGalc    = $( data ).attr( 'data-id-galc' );
        let arrTotal  = [];

        listaDasPortasPont.html( '' );
        comunicacao.html( '' );
        vocalizacao.html( '' );
        listaGalcGMON.html( '' );

        $( '#modalReporter' ).modal( 'show' );
        $( '.modal-title' ).html( 'Detalhes do GALC: <span class="label label-default"> ' + idGalc + '</span>' );
        

        listPortaPontByGalc( idGalc );
        

        arrTotal.push( parseInt( data_galc.TOTAL_7 ) );
        arrTotal.push( parseInt( data_galc.TOTAL_14 ) );
        arrTotal.push( parseInt( data_galc.TOTAL_21 ) );
        arrTotal.push( parseInt( data_galc.TOTAL_28 ) );
        arrTotal.push( parseInt( data_galc.TOTAL_35 ) );

        let avereageResult = checksTheAverageOfTheGalc( arrTotal );

        $( '#value-average' ).html( avereageResult );

        verifiedGalcINGmon( idGalc );
        
        qtdAlarmeDayByGalc( idGalc );
        qtdAlarmeLastHourByGalc( idGalc );
    };

    /**
     * Mostra acumulado do DIA dos alarmes por Galc
     * @param idGalc
     */
    qtdAlarmeDayByGalc = function ( idGalc ) {

        $.ajax( {
            url: '/qtd-alarme-day-by-galc',
            type: 'POST',
            data: idGalc,
            beforeSend: function () {
                no_dia.html( '...' );
            },
            success: function ( data ) {
                if ( data[ 'TOTAL' ] !== undefined ) {
                    no_dia.html( data[ 'TOTAL' ] );
                }
            }//end success
        } );
    };

    /**
     * Mostra acumulado na última hora dos alarmes por Galc
     * @param idGalc
     */
    qtdAlarmeLastHourByGalc = function ( idGalc ) {
        last_60min.removeClass( 'badge-error' );
        $.ajax( {
            url: '/qtd-alarme-last-hour-by-galc',
            type: 'POST',
            data: idGalc,
            beforeSend: function () {
               last_60min.html( '...' );
            },
            success: function ( data ) {
                if ( data[ 'LAST_HOUR' ] !== undefined ) {
                    last_60min.html( data[ 'LAST_HOUR' ] );
                    let total = parseInt( data[ 'LAST_HOUR' ] );
                    ( total > 0 ) ? last_60min.addClass( 'badge-error' ) : last_60min.removeClass( 'badge-error' )
                }
            }//end success
        } );
    };

    /**
     * Verifica se existe GALC no GMON nos últimos 10 min
     * @param idGalc
     */
    verifiedGalcINGmon = function ( idGalc ) {

        $.ajax( {
            url: '/verified-galc-in-gmond',
            type: 'POST',
            data: idGalc,
            beforeSend: function () {

            },
            success: function ( data ) {
                console.log('Alarme in top10: ', data);
                if ( data === 0 ) {
                    $( '#alarme_sicaweb' ).html( 'Não' ).removeClass( 'badge-error' );
                } else {
                    $( '#alarme_sicaweb' ).html( 'Sim' ).addClass( 'badge-error' );

                    // $( '#getListGalcGMON' ).on( 'click', function ( e ) {
                    //     e.preventDefault();
                    //     e.stopPropagation();

                        let htmlGMON = '';
                        htmlGMON += '';
                        htmlGMON += '<div class="alert alert-dismissible" role="alert">';
                        htmlGMON += `<h4>Alarmes presentes no SICAWEB (últimos 60min): <span class="label label-default"> ${ idGalc }</span> </h4>`;
                        htmlGMON += `<button type="button" class="close close-table" onclick="toggletable('table-galc-in-gmond')"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></button>`;
                        htmlGMON += '<div class="table-galc-in-gmond">';
                        htmlGMON += '<table class="table table-striped" width="100%">';
                        htmlGMON += '<thead>';
                        htmlGMON += '<tr class="tr-title">';
                        htmlGMON += '<td>Crítico</td>';
                        htmlGMON += '<td class="col-11">Data da Ocorrência</td>';
                        htmlGMON += '<td>DomainCode</td>';
                        htmlGMON += '<td>Info adicional</td>';
                        htmlGMON += '<td>Info Incidente</td>';
                        htmlGMON += '<td>Alias</td>';
                        htmlGMON += '<td>QTD</td>';
                        htmlGMON += '</tr>';
                        htmlGMON += '</thead>';
                        htmlGMON += '<tbody>';

                        $.each( data, function ( idx, vl ) {
                            htmlGMON += `<tr>`;

                            if ( vl.CRITICO == 'Sim' ) {
                                htmlGMON += `<td class="alarmeSicawebErro">${ ( vl.CRITICO ) ? vl.CRITICO : '' }</td>`;
                            } else if ( vl.CRITICO == 'Não' ) {
                                htmlGMON += `<td>${ ( vl.CRITICO ) ? vl.CRITICO : '' }</td>`;
                            }

                            htmlGMON += `<td>${ ( vl.DATA_OCORRENCIA ) ? vl.DATA_OCORRENCIA : '' }</td>`;
                            htmlGMON += `<td>${ ( vl.DOMAINCODE ) ? vl.DOMAINCODE : '-' }</td>`;
                            htmlGMON += `<td>${ ( vl.INFO_ADICIONAL ) ? vl.INFO_ADICIONAL : '' }</td>`;
                            htmlGMON += `<td>${ ( vl.INFO_INCIDENTE ) ? vl.INFO_INCIDENTE : '' }</td>`;
                            htmlGMON += `<td>${ ( vl.NODEALIAS ) ? vl.NODEALIAS : '' }</td>`;
                            htmlGMON += `<td><span class="badge">${ ( vl.QTD ) ? vl.QTD : 0 }</span></td>`;
                            htmlGMON += `</tr>`;
                        } );

                        htmlGMON += '</tbody>';
                        htmlGMON += '</table>';
                        htmlGMON += '</div>';
                        htmlGMON += '</div>';

                        listaGalcGMON.html( htmlGMON );
                    // } );
                }
            }//end success
        } );
    };

    openDetailGalcByState = function ( data ) {

        listaGalcGMONState.html( '' );
        listPortaPontByState.html( '' );
        comunicacaoUF.html( '' );
        vocalizacaoUF.html( '' );
        

        let data_galc = JSON.parse( $( data ).attr( 'data-galc' ) );
        let idGalc    = $( data ).attr( 'data-id-galc' );
        let uf        = $( data ).attr( 'data-uf' );
        let arrTotal  = [];

        getTopPage();

        $( '#modalPortaPontByState' ).modal( 'show' );
        $( '.modal-uf-title' ).html( 'Detalhes do GALC: <span class="label label-default"> ' + idGalc + '</span>' );

        arrTotal.push( parseInt( data_galc.TOTAL_7 ) );
        arrTotal.push( parseInt( data_galc.TOTAL_14 ) );
        arrTotal.push( parseInt( data_galc.TOTAL_21 ) );
        arrTotal.push( parseInt( data_galc.TOTAL_28 ) );
        arrTotal.push( parseInt( data_galc.TOTAL_35 ) );

        let avereageResult = checksTheAverageOfTheGalc( arrTotal );

        $( '#value-df-average' ).html( avereageResult );

        // $( '.getListPortaPontByState' ).on( 'click', function ( e ) {
        //     e.preventDefault();
        //     comunicacaoUF.html( '' );
        //     vocalizacaoUF.html( '' );
        //     baGalcUf.html( '' );
        //     listPortaPontByUf( uf, idGalc );
        // } );

        listPortaPontByUf( uf, idGalc );
        verifiedGalcINGmonByState( idGalc );

        qtdAlarmeDayByGalcUF( idGalc );
        qtdAlarmeLastHourByGalcUF( idGalc );
    };

    /**
     * Mostra acumulado do DIA dos alarmes por UF
     * @param idGalc
     */
    qtdAlarmeDayByGalcUF = function ( idGalc ) {

        $.ajax( {
            url: '/qtd-alarme-day-by-galc',
            type: 'POST',
            data: idGalc,
            beforeSend: function () {
                no_dia_uf.html( '...' );
            },
            success: function ( data ) {
                if ( data[ 'TOTAL' ] !== undefined ) {
                    no_dia_uf.html( data[ 'TOTAL' ] );
                }
            }//end success
        } );
    };

    /**
     * Mostra acumulado na última hora dos alarmes por Galc UF
     * @param idGalc
     */
    qtdAlarmeLastHourByGalcUF = function ( idGalc ) {
        last_60min_uf.removeClass( 'badge-error' );
        $.ajax( {
            url: '/qtd-alarme-last-hour-by-galc',
            type: 'POST',
            data: idGalc,
            beforeSend: function () {
                last_60min_uf.html( '...' );
            },
            success: function ( data ) {
                if ( data[ 'LAST_HOUR' ] !== undefined ) {
                    last_60min_uf.html( data[ 'LAST_HOUR' ] );
                    let total = parseInt( data[ 'LAST_HOUR' ] );
                    ( total > 0 ) ? last_60min_uf.addClass( 'badge-error' ) : last_60min_uf.removeClass( 'badge-error' )
                }
            }//end success
        } );
    };
    

    /**
     * Verifica se existe GALC no GMON por Estado nos últimos 10 min
     * @param idGalc
     */
    verifiedGalcINGmonByState = function ( idGalc ) {
        $.ajax( {
            url: '/verified-galc-in-gmond',
            type: 'POST',
            data: idGalc,
            beforeSend: function () {

            },
            success: function ( data ) {
                console.log('Alarme by State: ', data);
                if ( data === 0 ) {
                    $( '#alarme_sicaweb_df' ).html( 'Não' ).removeClass( 'badge-error' );
                } else {
                    // $( '#alarme_sicaweb_df' ).html( '<a href="#" id="getListGalcGMONState">Sim</a>' ).addClass( 'badge-error' );
                    $( '#alarme_sicaweb_df' ).html( 'Sim' ).addClass( 'badge-error' );

                    // $( '#getListGalcGMONState' ).on( 'click', function ( e ) {
                    //     e.preventDefault();
                    //     e.stopPropagation();

                        let htmlGMON = '';
                        htmlGMON += '';
                        htmlGMON += '<div class="alert alert-dismissible" role="alert">';
                        htmlGMON +=  `<h4>Alarmes presentes no SICAWEB (últimos 60min): <span class="label label-default"> ${ idGalc }</span></h4>`;
                        htmlGMON += `<button type="button" class="close close-table" onclick="toggletable('table-galc-in-gmond-uf')"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></button>`;
                        htmlGMON += '<div class="table-galc-in-gmond-uf">';
                        htmlGMON += '<table class="table table-striped" width="100%">';
                        htmlGMON += '<thead>';
                        htmlGMON += '<tr class="tr-title">';
                        htmlGMON += '<td>Crítico</td>';
                        htmlGMON += '<td>Data da Ocorrência</td>';
                        htmlGMON += '<td>DomainCode</td>';
                        htmlGMON += '<td>GalcOLT</td>';
                        htmlGMON += '<td>Info adicional</td>';
                        htmlGMON += '<td>Info Incidente</td>';
                        htmlGMON += '<td>Alias</td>';
                        htmlGMON += '<td>QTD</td>';
                        htmlGMON += '</tr>';
                        htmlGMON += '</thead>';
                        htmlGMON += '<tbody>';

                        $.each( data, function ( idx, vl ) {
                            htmlGMON += `<tr>`;
                            // htmlGMON += `<td>${ ( vl.CRITICO ) ? vl.CRITICO : '' }</td>`;

                            if ( vl.CRITICO == 'Sim' ) {
                                htmlGMON += `<td class="alarmeSicawebErro">${ ( vl.CRITICO ) ? vl.CRITICO : '' }</td>`;
                            } else if ( vl.CRITICO == 'Não' ) {
                                htmlGMON += `<td>${ ( vl.CRITICO ) ? vl.CRITICO : '' }</td>`;
                            }

                            htmlGMON += `<td>${ ( vl.DATA_OCORRENCIA ) ? vl.DATA_OCORRENCIA : '' }</td>`;
                            htmlGMON += `<td>${ ( vl.DOMAINCODE ) ? vl.DOMAINCODE : '' }</td>`;
                            htmlGMON += `<td>${ ( vl.GALC_OLT ) ? vl.GALC_OLT : '' }</td>`;
                            htmlGMON += `<td>${ ( vl.INFO_ADICIONAL ) ? vl.INFO_ADICIONAL : '' }</td>`;
                            htmlGMON += `<td>${ ( vl.INFO_INCIDENTE ) ? vl.INFO_INCIDENTE : '' }</td>`;
                            htmlGMON += `<td>${ ( vl.NODEALIAS ) ? vl.NODEALIAS : '' }</td>`;
                            htmlGMON += `<td><span class="badge">${ ( vl.QTD ) ? vl.QTD : 0 }</span></td>`;
                            htmlGMON += `</tr>`;
                        } );

                        htmlGMON += '</tbody>';
                        htmlGMON += '</table>';
                        htmlGMON += '</div>';
                        htmlGMON += '</div>';

                        listaGalcGMONState.html( htmlGMON );
                   // } );
                }
            }//end success
        } );
    };
    
    /**
     * Mostra o detalhamento do Top10 GALC de cada media x dia (7, 14, 21, 28, 35 dias)
     *
     */
    openDetailGalcBySpecificDay = function ( data ) {
        let html                  = '';
        // let dataGalc = $( data ).attr( 'data-galc' );
        let dataGalc              = JSON.parse( $( data ).attr( 'data-galc' ) );
        let dataDays              = JSON.parse( $( data ).attr( 'data-days' ) );
        let tbBodyDetailGalcByDay = $( '#tbody_detail-galc-by-day' );


        $( '.modal-title' ).html( 'Detalhes do GALC: <span class="label label-default"> ' + dataGalc + '</span>' );

        $.ajax( {
            url: '/detail-galc-by-specific-day',
            type: 'POST',
            data: dataGalc + '&' + dataDays,
            beforeSend: function () {
                tbBodyDetailGalcByDay.html( '' );
                $( '#msg-detail-galc-by-day' ).html( `Procurando o detalhamento do Galc.  Favor aguarde...` ).show();
                $( '#modalDetailGalcByDay' ).modal( 'show' );
            },
            success: function ( data ) {

                $( '#msg-detail-galc-by-day' ).html( '' ).hide();

                $.each( data, function ( idx, vl ) {
                    html += `<tr>`;
                    html += `<td>${ vl.GALC_OLT }</td>`;
                    html += `<td>${ vl.UF }</td>`;
                    html += `<td>${ vl.LOCALIDADE }</td>`;
                    html += `<td>${ vl.MUNICIPIO }</td>`;
                    html += `<td>${ vl.PORTA_PON }</td>`;
                    html += `<td>${ ( vl.BA ) ? vl.BA : '' }</td>`;
                    html += `<td>${ vl.TIPO_EVENTO }</td>`;
                    html += `<td>${ vl.QTD }</td>`;
                    html += `</tr>`;
                } );

                tbBodyDetailGalcByDay.append( html );
            }//end success
        } );
    };

    /**
     * Mostra as portas PON por GALC da lista do top 10
     * @param idGalc
     */
    listPortaPontByGalc = function ( idGalc ) {
        listaDasPortasPont.empty();
        comunicacao.html( '' );
        vocalizacao.html( '' );
        baGalc.html( '' );

        let isComunicacao = [];
        let isVocalizacao = [];
        let isBA          = [];

        $.ajax( {
            url: '/list-porta-pont-by-galc',
            type: 'POST',
            data: idGalc,
            beforeSend: function () {
                msgPortaPont.html( `Procurando as Porta Pont do galc: ${ idGalc }. Favor aguarde...` ).show();
            },
            success: function ( data ) {
                msgPortaPont.html( '' ).hide();
                let qtd_porta_pont = 0;
                let html           = '';
                html += '<div class="alert alert-dismissible" role="alert">';
                html +=  `<h4>Portas Pon do Galc (últimos 60min): <span class="label label-default"> ${ idGalc }</span></h4>`;
                html += `<button type="button" class="close close-table" onclick="toggletable('table-pont-by-galc')"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></button>`;
                html += '<div class="table-pont-by-galc">';
                html += '<table class="table table-striped" width="100%">';
                html += '<thead>';
                html += '<tr class="tr-title">';
                html += '<td class="col-11">Dt Consulta</td>';
                html += '<td>Porta PON</td>';
                html += '<td>BA</td>';
                html += '<td>UF</td>';
                html += '<td>Município</td>';
                html += '<td>Localidade</td>';
                html += '<td>Tipo evento</td>';
                html += '<td>QTD</td>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';

                $.each( data[ 'listPostaPontByGalc' ], function ( idx, vl ) {
                    qtd_porta_pont += parseInt( vl.QTD );

                    html += `<tr>`;
                    html += `<td>${ ( vl.DT_HH_CONSULTA ) ? vl.DT_HH_CONSULTA : '' }</td>`;
                    html += `<td>${ ( vl.PORTA_PON ) ? vl.PORTA_PON : '' }</td>`;
                    html += `<td>${ ( vl.BA ) ? vl.BA : '' }</td>`;
                    html += `<td>${ ( vl.UF ) ? vl.UF : '' }</td>`;
                    html += `<td>${ ( vl.MUNICIPIO ) ? vl.MUNICIPIO : '' }</td>`;
                    html += `<td>${ ( vl.LOCALIDADE ) ? vl.LOCALIDADE : '' }</td>`;
                    html += `<td>${ ( vl.TIPO_EVENTO ) ? vl.TIPO_EVENTO : '' }</td>`;
                    html += `<td><span class="badge">${ ( vl.QTD ) ? vl.QTD : 0 }</span></td>`;
                    html += `</tr>`;

                    if ( vl.NUM_EVENTO == null ) {
                        isComunicacao.push( 0 );
                        isVocalizacao.push( 0 );
                    } else if ( vl.NUM_EVENTO !== null && vl.NUM_EVENTO == vl.GALC_OLT ) {
                        isVocalizacao.push( 1 );
                    } else if ( vl.NUM_EVENTO !== null && vl.NUM_EVENTO != vl.GALC_OLT ) {
                        isComunicacao.push( 1 );
                    }

                    if ( vl.BA == null ) {
                        isBA.push( 0 );
                    } else{
                        isBA.push( 1 );
                    }
                } );

                html += '</tbody>';
                html += '</table>';
                html += '</div>';
                html += '</div>';

                $( '#spanListaPon' ).html(qtd_porta_pont);

                if ( $.inArray( 1, isComunicacao ) === -1 ) {
                    comunicacao.append( 'Não' ).removeClass( 'badge-error' ).addClass( 'label-default' );
                } else {
                    comunicacao.append( 'Sim' ).addClass( 'badge-error' );
                }

                if ( $.inArray( 1, isVocalizacao ) === -1 ) {
                    vocalizacao.append( 'Não' ).removeClass( 'badge-error' ).addClass( 'label-default' );
                } else {
                    vocalizacao.append( 'Sim' ).addClass( 'badge-error' );
                }

                if ( $.inArray( 1, isBA ) === -1 ) {
                    baGalc.append( 'Não' ).removeClass( 'badge-error' ).addClass( 'label-default' );
                } else {
                    baGalc.append( 'Sim' ).addClass( 'badge-error' );
                }

                listaDasPortasPont.html( html );
            }//end success
        } );
    };

    /**
     * Mostra a porta pon por UF
     * @param uf
     */
    listPortaPontByUf = function ( uf, idGalc ) {
        listPortaPontByState.empty();
        comunicacaoUF.html( '' );
        vocalizacaoUF.html( '' );
        baGalcUf.html( '' );
        

        let isComunicacaoUF = [];
        let isVocalizacaoUF = [];
        let isBaGalcUF      = [];
        let html            = '';
        $.ajax( {
            url: '/list-porta-pont-by-state',
            type: 'POST',
            data: idGalc,
            beforeSend: function () {
                msgPortaPontByState.html( `Procurando as Portas Pont do galc: ${ idGalc }.  Favor aguarde...` ).show();
            },
            success: function ( data ) {
                msgPortaPontByState.html( '' ).hide();
                let qtd_porta_pont = 0;
                html += '<div class="alert alert-dismissible" role="alert">';
                html +=  `<h4>Portas Pon do Galc (últimos 60min): <span class="label label-default"> ${ idGalc }</span></h4>`;
                html += `<button type="button" class="close close-table" onclick="toggletable('table-pon-by-uf')"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></button>`;
                html += '<div class="table-pon-by-uf">';
                html += '<table class="table table-striped" width="100%">';
                html += '<table class="table table-striped" width="100%">';
                html += '<thead>';
                html += '<tr class="tr-title">';
                html += '<td class="col-11">DT Consulta</td>';
                html += '<td>Porta PON</td>';
                html += '<td>BA</td>';
                html += '<td>Galc</td>';
                html += '<td>UF</td>';
                html += '<td>Município</td>';
                html += '<td>Localidade</td>';
                html += '<td>Tipo evento</td>';
                html += '<td>QTD</td>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';

                $.each( data[ 'listPostaPontByState' ], function ( idx, vl ) {
                    qtd_porta_pont += parseInt( vl.QTD );
                    html += `<tr>`;
                    html += `<td>${ ( vl.DT_HH_CONSULTA ) ? vl.DT_HH_CONSULTA : '' }</td>`;
                    html += `<td>${ ( vl.PORTA_PON ) ? vl.PORTA_PON : '' }</td>`;
                    html += `<td>${ ( vl.BA ) ? vl.BA : '' }</td>`;
                    html += `<td>${ ( vl.GALC_OLT ) ? vl.GALC_OLT : '' }</td>`;
                    html += `<td>${ ( vl.UF ) ? vl.UF : '' }</td>`;
                    html += `<td>${ ( vl.MUNICIPIO ) ? vl.MUNICIPIO : '' }</td>`;
                    html += `<td>${ ( vl.LOCALIDADE ) ? vl.LOCALIDADE : '' }</td>`;
                    html += `<td>${ ( vl.TIPO_EVENTO ) ? vl.TIPO_EVENTO : '' }</td>`;
                    html += `<td><span class="badge">${ vl.QTD }</span></td>`;
                    html += `</tr>`;

                    if ( vl.NUM_EVENTO == null ) {
                        isComunicacaoUF.push( 0 );
                        isVocalizacaoUF.push( 0 );
                    } else if ( vl.NUM_EVENTO !== null && vl.NUM_EVENTO == vl.GALC_OLT ) {
                        isVocalizacaoUF.push( 1 );
                    } else if ( vl.NUM_EVENTO !== null && vl.NUM_EVENTO != vl.GALC_OLT ) {
                        isComunicacaoUF.push( 1 );
                    }

                    if ( vl.BA == null ) {
                        isBaGalcUF.push( 0 );
                    } else {
                        isBaGalcUF.push( 1 );
                    }
                } );

                html += '</tbody>';
                html += '</table>';
                html += '</div>';
                html += '</div>';

                $( '#spanListaPonByUf' ).html( qtd_porta_pont );

                if ( $.inArray( 1, isComunicacaoUF ) === -1 ) {
                    comunicacaoUF.html( 'Não' ).removeClass( 'badge-error' ).addClass( 'label-default' );
                } else {
                    comunicacaoUF.html( 'Sim' ).addClass( 'badge-error' );
                }

                if ( $.inArray( 1, isVocalizacaoUF ) === -1 ) {
                    vocalizacaoUF.html( 'Não' ).removeClass( 'badge-error' ).addClass( 'label-default' );
                } else {
                    vocalizacaoUF.html( 'Sim' ).addClass( 'badge-error' );
                }

                if ( $.inArray( 1, isBaGalcUF ) === -1 ) {
                    baGalcUf.html( 'Não' ).removeClass( 'badge-error' ).addClass( 'label-default' );
                } else {
                    baGalcUf.html( 'Sim' ).addClass( 'badge-error' );
                }

                listPortaPontByState.html( html );

            }//end success
        } );
    };

    getTopPage = function () {
        $( 'html, body' ).animate( { scrollTop: 0 }, 'slow' );
    };

    /**
     * Calcula a media das ultimas 5 semanas
     * @param arrTotal
     * @returns {*}
     */
    function checksTheAverageOfTheGalc( arrTotal ) {
        let arrLength = arrTotal.length;
        let media     = 0;

        if ( arrLength > 0 ) {
            arrTotal.sort( sortfunction );
            arrTotal.pop();
            arrTotal.shift();

            let arrSum = arrTotal.reduce( sum, 0 );
            media      = arrSum / 3;
            media      = parseFloat( media.toFixed( 2 ) );
            media      = media.toString().replace( ".", "," );

            return media;
        }

        return media;
    }

    /**
     * Soma todos os valores do array passado
     * @param value
     * @param a
     * @returns {*}
     */
    function sum( value, a ) {
        return value + a;
    }

    /**
     * Orderna em forma crescente
     * @param a
     * @param b
     * @returns {number}
     */
    function sortfunction( a, b ) {
        return ( a - b )
    }

    $( '.btn-fechar-modal' ).on( 'click', function ( e ) {
        comunicacao.html( '' );
        vocalizacao.html( '' );
        $( '.comunicacao_uf' ).html( '' );
        $( '.vocalizacao_uf' ).html( '' );
        listaDasPortasPont.html( '' );
        $( '#listPortaPontByState' ).html( '' );
    } );

    function createHeaderAverage() {
        // $( ".min10" ).append( moment().subtract( 10, 'minutes' ).format( 'H:mm' ) );
        $( ".mediaDetachment" ).append( 'Deslocamento<br /> da Média' );
        $( ".average" ).append( 'Média Hist.' );
        $( ".min10" ).append( 'Ult. 60 min' );
        $( ".days7" ).append( moment().subtract( 7, 'days' ).format( 'D/MM' ) );
        $( ".days14" ).append( moment().subtract( 14, 'days' ).format( 'D/MM' ) );
        $( ".days21" ).append( moment().subtract( 21, 'days' ).format( 'D/MM' ) );
        $( ".days28" ).append( moment().subtract( 28, 'days' ).format( 'D/MM' ) );
        $( ".days35" ).append( moment().subtract( 35, 'days' ).format( 'D/MM' ) );
    }
    
    toggletable = function ( class_div ) {
        $( `.${ class_div }` ).toggle( "slow" ); //slow | fast
    }

} );
