$(document).ready(function(){

   var tblPessoas = $('#tblPessoas');

   tblPessoas.on('click', '#btn_deletar', function(e){
      e.preventDefault();

      var id = $(this).attr('data-id');

      bootbox.confirm("Deseja deletar o registro?", function(resul){
         if( resul == true ){
            $.ajax({
               url: '/pessoa/deletar/'+id,
               type: 'DELETE',
               success: function(data){
                  if( data == 'deletou' ){
                     bootbox.dialog({
                        message: "Registro deletado com sucess!",
                        title: "CRUD com Slim + PDO",
                        buttons:{
                           success: {
                              label: "Fechar janela",
                              className: "btn btn-success",
                              callback: function(){
                                 $(location).attr('href','/pessoas');
                              }
                           }
                        }
                     })
                  }else if( data == 'ndeletou'){
                     bootbox.dialog({
                        message: "Erro ao tentar deletar o registro!",
                        title: "CRUD com Slim + PDO",
                        buttons:{
                           success: {
                              label: "Fechar janela",
                              className: "btn btn-danfer",
                              callback: function(){
                                 $(location).attr('href','/pessoas');
                              }
                           }
                        }
                     })
                  }
               }
            })
         }
      });
   });
});