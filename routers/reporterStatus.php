<?php

$app->post( '/galc_by_state', function () use ( $app, $connFTTH ) {

    $idState = strtoupper( $app->request()->getBody() );

    $listGalcByState = getGalcOltByState( $connFTTH, $idState );

    $dataResult = [
        'listGalcByState' => $listGalcByState,
    ];

    prepareJsonResponse( $app, $dataResult );

} )
    ->name( 'listGalcByState' );


$app->post( '/average-check', function () use ( $app, $connFTTH ) {

    $arrStatesWithGalc = [];
    $listFtth          = searchDataByState( $connFTTH );
    $resultAverage     = checksTheAverageOfTheGalcByState( $listFtth );
    
    $states = array(
        'AC',
        'AL',
        'AP',
        'AM',
        'BA',
        'CE',
        'DF',
        'ES',
        'GO',
        'MA',
        'MT',
        'MS',
        'MG',
        'PA',
        'PB',
        'PR',
        'PE',
        'PI',
        'RJ',
        'RN',
        'RS',
        'RO',
        'RR',
        'SC',
        'SP',
        'SE',
        'TO',
    );


    $top10Galc = getTop10InArray( $resultAverage );

    /**
     * Agrupa os valores do GALC por cada estado
     */
    foreach ( $resultAverage as $key => $value ) {
        foreach ( $states as $k => $v ) {
            if ( $v == $value[ 'UF' ] ) {
                $arrStatesWithGalc[ $v ][] = $value;
            }
        }
    }
    

    /**
     * Ordena o array pelo > para <  media_detachment_sem_format
     */
    foreach ($arrStatesWithGalc as $key => $value):
        usort( $arrStatesWithGalc[$key], function ( $a, $b ) {
            return (float)$a[ 'media_detachment_sem_format' ] < (float)$b[ 'media_detachment_sem_format' ];
        } );
    endforeach;
    
    $dataResult = [
        'resultAverage' => $arrStatesWithGalc,
        'top10Galc'     => $top10Galc,
    ];


    prepareJsonResponse( $app, $dataResult );

} )
    ->name( 'averageCheck' );


$app->post( '/top-galc-10', function () use ( $app, $connFTTH ) {

    $top10GalcOlt = getTop10GalcOlt( $connFTTH );

    $dataResult = [
        'top10GalcOlt' => $top10GalcOlt,
    ];

    prepareJsonResponse( $app, $dataResult );

} )
    ->name( 'top10Galc' );

$app->post( '/list-porta-pont-by-galc', function () use ( $app, $connFTTH ) {

    $idGalc = strtoupper( $app->request()->getBody() );

    $listPortaPontByGalc = getQtdPortaPontByGalc( $connFTTH, $idGalc );

    foreach ( $listPortaPontByGalc as $k => $list ):
        $listPortaPontByGalc[ $k ][ 'DT_HH_CONSULTA' ] = dateHoraMinuteBR( $list[ 'DT_HH_CONSULTA' ] );
    endforeach;


    $dataResult = [
        'listPostaPontByGalc' => $listPortaPontByGalc,
    ];

    prepareJsonResponse( $app, $dataResult );

} )
    ->name( 'listPostaPontByGalc' );

$app->post( '/list-porta-pont-by-state', function () use ( $app, $connFTTH ) {

    $idGalc = strtoupper( $app->request()->getBody() );

    $listPortaPontByState = getQtdPortaPontByState( $connFTTH, $idGalc );

    if ( $listPortaPontByState > 0 ) {
        foreach ( $listPortaPontByState as $k => $list ):
            $listPortaPontByState[ $k ][ 'DT_HH_CONSULTA' ] = dateHoraMinuteBR( $list[ 'DT_HH_CONSULTA' ] );
        endforeach;
    }

    $dataResult = [
        'listPostaPontByState' => $listPortaPontByState,
    ];

    prepareJsonResponse( $app, $dataResult );

} )
    ->name( 'listPostaPontByState' );


$app->post( '/detail-galc-by-specific-day', function () use ( $app, $connFTTH ) {

    $arrDataGalc = explode( '&', $app->request()->getBody() );
    $galcName    = $arrDataGalc[ 0 ];
    $qtdDays     = $arrDataGalc[ 1 ];

    $detailGalcBySpecificDay = getDetailGalcBySpecificDays( $galcName, $qtdDays, $connFTTH );

    prepareJsonResponse( $app, $detailGalcBySpecificDay );

} )
    ->name( 'detailGalcBySpecificDay' );


$app->post( '/verified-galc-in-gmond', function () use ( $app, $connGMON ) {

    $galcName = strtoupper( $app->request()->getBody() );

    $dataGalcGmon = getDataGalcInGmon( $connGMON, $galcName );

    if ( $dataGalcGmon > 0 ) {
        foreach ( $dataGalcGmon as $k => $list ):
            $dataGalcGmon[ $k ][ 'DATA_OCORRENCIA' ] = dateHoraMinuteBR( $list[ 'DATA_OCORRENCIA' ] );
        endforeach;
    }

    prepareJsonResponse( $app, $dataGalcGmon );

} )
    ->name( 'verifiedGalcInGmond' );

$app->post( '/qtd-alarme-day-by-galc', function () use ( $app, $connGMON ) {

    $galcName = strtoupper( $app->request()->getBody() );

    $qtdInDay = getQtdAlarmeDayByGalc( $connGMON, $galcName );

    prepareJsonResponse( $app, $qtdInDay );

} )
    ->name( 'qtdAlarmeDayByGalc' );

$app->post( '/qtd-alarme-last-hour-by-galc', function () use ( $app, $connGMON ) {

    $galcName = strtoupper( $app->request()->getBody() );

    $qtdlastHour = getQtdAlarmeLastHourByGalc( $connGMON, $galcName );

    prepareJsonResponse( $app, $qtdlastHour );

} )
    ->name( 'qtdAlarmeDayByGalc' );


/**
 * @param $arrData
 * Retorna o 10 primeiros dados do array ordenados pelo maior valor
 *
 * @return array
 */

function getTop10InArray( $arrData )
{
    $arrTop10 = [];

    usort( $arrData, function ( $a, $b ) {
        return (float)$a[ 'media_detachment_sem_format' ] < (float)$b[ 'media_detachment_sem_format' ];
    } );
    
    if( count($arrData) > 0 ){
        for ( $i = 0; $i < 10; $i++ ) {
            array_push( $arrTop10, $arrData[ $i ] );
        }
    }
 
    return $arrTop10;
}


/**
 * Mostra os Top 10 Galc dos últimos 60 min
 */
function getDataGalcInGMON( $connGMON, $galcName )
{

    $sql = "
        SELECT
            node AS GALC_OLT,
            nodealias,
            CASE WHEN url LIKE '%#CI=S%' THEN 'Sim' ELSE 'Não' END AS CRITICO,
            TO_CHAR(max(firstoccurrence), 'YYYY-MM-DD HH24:MI:SS') AS DATA_OCORRENCIA,
            domaincode,
            additionaltext AS INFO_ADICIONAL,
            addinfoincidente AS INFO_INCIDENTE,
            COUNT(DISTINCT additionaltext) QTD
        FROM ITXDB_AGREGACAO.REPORTER_STATUS
        WHERE node = '{$galcName}'
        --AND url LIKE '%#CI=S%'--FILTRAR APENAS CRITICOS
        AND firstoccurrence > SYSDATE - 60 / 24 / 60
        GROUP BY node, nodealias, CASE WHEN url LIKE '%#CI=S%' THEN 'Sim' ELSE 'Não' END, domaincode, additionaltext, addinfoincidente
        ORDER BY max(firstoccurrence) DESC, QTD DESC
    ";

    $query = $connGMON->prepare( $sql );

    $query->execute();
    $resultList = $query->fetchAll( \PDO::FETCH_ASSOC );
    $connGMON->close();

    return ( count( $resultList ) ) ? $resultList : 0;
}


/**
 * Mostra o acumulado de alarmes no dia
 */
function getQtdAlarmeDayByGalc( $connGMON, $galcName )
{
    $sql = "
         SELECT
          count(firstoccurrence) as total
         FROM ITXDB_AGREGACAO.REPORTER_STATUS
         WHERE node = '{$galcName}'
         AND trunc(firstoccurrence) = TRUNC(SYSDATE)
    ";

    $query = $connGMON->prepare( $sql );

    $query->execute();
    $result = $query->fetchAll( \PDO::FETCH_ASSOC );
    $connGMON->close();

    return $result[0];
}
/**
 * Mostra o acumulado de alarmes na última hora
 */
function getQtdAlarmeLastHourByGalc( $connGMON, $galcName )
{
    $sql = "
        SELECT
            count(firstoccurrence) AS last_hour
            FROM ITXDB_AGREGACAO.REPORTER_STATUS
            WHERE node = '{$galcName}'
        AND firstoccurrence >= SYSDATE - 60/(24*60)
    ";

    $query = $connGMON->prepare( $sql );

    $query->execute();
    $result = $query->fetchAll( \PDO::FETCH_ASSOC );
    $connGMON->close();

    return $result[0];
}


/**
 * Mostra os Top 10 Galc dos últimos 60 min
 */
function getTop10GalcOlt( $connFTTH )
{
    $sqltop10 = "
       SELECT * FROM (
				SELECT
					GALC_OLT,
					--TO_CHAR(DT_HH_CONSULTA,'yyyy-mm-dd hh24:mi:ss' ) DT_HH_CONSULTA,
				    COUNT(*) QTD
				FROM processos_ftth
				WHERE DT_HH_CONSULTA > SYSDATE - 60/24/60
				AND GALC_OLT IS NOT NULL
				GROUP BY GALC_OLT
				--, TO_CHAR(DT_HH_CONSULTA,'yyyy-mm-dd hh24:mi:ss' )
				ORDER BY QTD DESC
	) WHERE ROWNUM <= 10
    ";

    $query = $connFTTH->prepare( $sqltop10 );

    $query->execute();
    $top10GalcList = $query->fetchAll( \PDO::FETCH_ASSOC );
    $connFTTH->close();

    $historicalAverage = calculatesTheHistoricalAverage( $top10GalcList, $connFTTH );

    return $historicalAverage;
}


/**
 * Mostra os Galc  dos últimos 10 min por Estado
 */
function getGalcOltByState( $connFTTH, $state )
{
    $sqlGalcByState = "
            SELECT 
                    GALC_OLT,
                    --DT_HH_CONSULTA,
                    COUNT(*) QTD 
            FROM processos_ftth  
            WHERE 
            DT_HH_CONSULTA >= SYSDATE - 60/24/60 
            AND UF ='{$state}'
            GROUP BY GALC_OLT
            --, DT_HH_CONSULTA
            ORDER BY QTD DESC
    ";

    $query = $connFTTH->prepare( $sqlGalcByState );

    $query->execute();
    $top10GalcList = $query->fetchAll( \PDO::FETCH_ASSOC );
    $connFTTH->close();

    $historicalAverage = calculatesTheHistoricalAverage( $top10GalcList, $connFTTH );

    return $historicalAverage;
}


function calculatesTheHistoricalAverage( $listGalcs, $connFTTH )
{
    $arrAverage = [];

    foreach ( $listGalcs as $key => $list ):
        $galc_olt             = $list[ 'GALC_OLT' ];
        $qdtTotalGalcDiaAtual = $list[ 'QTD' ];

        $resultDay1 = getCountGalcByDays( $galc_olt, 7, $connFTTH, $qdtTotalGalcDiaAtual );
        $resultDay2 = getCountGalcByDays( $galc_olt, 14, $connFTTH, $qdtTotalGalcDiaAtual );
        $resultDay3 = getCountGalcByDays( $galc_olt, 21, $connFTTH, $qdtTotalGalcDiaAtual );
        $resultDay4 = getCountGalcByDays( $galc_olt, 28, $connFTTH, $qdtTotalGalcDiaAtual );
        $resultDay5 = getCountGalcByDays( $galc_olt, 35, $connFTTH, $qdtTotalGalcDiaAtual );

        $arrAverage[ $key ][ $galc_olt ][ 'dia_1' ]                           = $resultDay1;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_1' ][ 'number_previous_days' ] = 7;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_2' ]                           = $resultDay2;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_2' ][ 'number_previous_days' ] = 14;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_3' ]                           = $resultDay3;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_3' ][ 'number_previous_days' ] = 21;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_4' ]                           = $resultDay4;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_4' ][ 'number_previous_days' ] = 28;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_5' ]                           = $resultDay5;
        $arrAverage[ $key ][ $galc_olt ][ 'dia_5' ][ 'number_previous_days' ] = 35;
    endforeach;

    return $arrAverage;
}

function subData( $dataString, $qtdDays )
{
    $data          = $dataString . ':00:00';
    $dataConverter = date( 'Y-m-d H:i:s', strtotime( $qtdDays, strtotime( $data ) ) );

    return $dataConverter;
}


function checksTheAverageOfTheGalcByHistorical( $list )
{

}

function prepareDataTop10( $arrAverage )
{
    $table = [];

    foreach ( $arrAverage as $k => $v ):
        foreach ( $v as $p1 => $p2 ):
            $table[ $p1 ] = $p2;
        endforeach;
    endforeach;


    return $table;
}


/**
 * @param $galc_olt
 * @param $qtd_dias
 * @param $connBD
 *
 * Retorna o detalhamento do Galc por dia especifico
 *
 * @return int
 */
function getDetailGalcBySpecificDays( $galc_olt, $qtd_dias, $connBD )
{

    if($qtd_dias == 1){
       $between = "BETWEEN ( SYSDATE - 1/24 ) AND SYSDATE";
    }else{
        $between = "BETWEEN ( SYSDATE - {$qtd_dias} ) - 60/24/60 AND SYSDATE - {$qtd_dias}";
    }
    

    $sql = "
        SELECT p.PORTA_PON,p.BA, p.GALC_OLT, p.UF, p.MUNICIPIO, p.LOCALIDADE, p.NUM_EVENTO,
	    TO_CHAR(MAX(p.DT_HH_CONSULTA),'YYYY-MM-DD HH24:MI:SS') DT_HH_CONSULTA,p.TIPO_EVENTO, COUNT(*) QTD
        FROM PROCESSOS_FTTH p 
        WHERE p.GALC_OLT = '{$galc_olt}'
        AND DT_HH_CONSULTA {$between}
        GROUP BY p.PORTA_PON,p.BA, p.GALC_OLT, p.UF, p.MUNICIPIO, p.LOCALIDADE, p.NUM_EVENTO,p.TIPO_EVENTO
        ORDER BY QTD DESC
    ";
     
    
    $query = $connBD->prepare( $sql );

    $query->execute();
    $resultList = $query->fetchAll( \PDO::FETCH_ASSOC );
    $connBD->close();

    return ( count( $resultList ) ) ? $resultList : 0;
}

/**
 * @param $galc_olt
 * @param $dd_hh_consulta
 * @param $connBD
 * Retorna a quantidade de Galc por dia específico
 *
 * @return mixed
 * @throws Exception
 */
function getCountGalcByDays( $galc_olt, $qtd_dias, $connBD, $qdtTotalGalcDiaAtual )
{
    $sql   = "SELECT 
                GALC_OLT,
                count(*) total
                FROM PROCESSOS_FTTH
              WHERE  
              GALC_OLT = '{$galc_olt}'
               AND DT_HH_CONSULTA BETWEEN ( SYSDATE - {$qtd_dias} ) - 60/24/60 AND SYSDATE - {$qtd_dias}
              GROUP BY GALC_OLT
            ";
    $query = $connBD->prepare( $sql );

    $query->execute();
    $resultList = $query->fetchAll( \PDO::FETCH_ASSOC );
    $connBD->close();

    $resultList[ 0 ][ 'qdtTotalGalcDiaAtual' ] = $qdtTotalGalcDiaAtual;

    return ( count( $resultList ) ) ? $resultList[ 0 ] : 0;
}


function searchDataByState( $connFTTH )
{
    $sql2 = "
        WITH galcs AS (
            SELECT
                uf,
                galc_olt,
                COUNT(DISTINCT CLIENTE) total
            FROM
                processos_ftth
            WHERE
                dt_hh_consulta > SYSDATE - 1 / 24
                AND galc_olt IS NOT NULL
            GROUP BY
                uf,
                galc_olt) SELECT
                t1.*,
                (
                    SELECT COUNT(DISTINCT CLIENTE)
                FROM
                    processos_ftth t2
                WHERE
                    t1.galc_olt = t2.galc_olt
                    AND dt_hh_consulta BETWEEN (SYSDATE - 7) - 1 / 24 AND SYSDATE - 7
                    AND galc_olt IS NOT NULL) total_7,
                (
                    SELECT COUNT(DISTINCT CLIENTE)
                FROM
                    processos_ftth t2
                WHERE
                    t1.galc_olt = t2.galc_olt
                    AND dt_hh_consulta BETWEEN (SYSDATE - 14) - 1 / 24 AND SYSDATE - 14
                    AND galc_olt IS NOT NULL) total_14,
                (
                    SELECT COUNT(DISTINCT CLIENTE)
                FROM
                    processos_ftth t2
                WHERE
                    t1.galc_olt = t2.galc_olt
                    AND dt_hh_consulta BETWEEN (SYSDATE - 21) - 1 / 24 AND SYSDATE - 21
                    AND galc_olt IS NOT NULL) total_21,
                (
                    SELECT COUNT(DISTINCT CLIENTE)
                FROM
                    processos_ftth t2
                WHERE
                    t1.galc_olt = t2.galc_olt
                    AND dt_hh_consulta BETWEEN (SYSDATE - 28) - 1 / 24 AND SYSDATE - 28
                    AND galc_olt IS NOT NULL) total_28,
                (
                    SELECT COUNT(DISTINCT CLIENTE)
                FROM
                    processos_ftth t2
                WHERE
                    t1.galc_olt = t2.galc_olt
                    AND dt_hh_consulta BETWEEN (SYSDATE - 35) - 1 / 24 AND SYSDATE - 35
                    AND galc_olt IS NOT NULL) total_35
            FROM
                galcs t1
            ORDER BY
                total DESC
    ";

    $query = $connFTTH->prepare( $sql2 );

    $query->execute();
    $statusListFtth = $query->fetchAll( \PDO::FETCH_ASSOC );

    $connFTTH->close();

    return $statusListFtth;
}


/*
 * Função para check na media para as cores do mapa seguindo a regra:
 * Para as cores do mapa vamos considerar cor:
 *   verde para uma quantidade de ligações menores ou iguais a média,
 *   amarelo para até 20% de descolamento da média
 *   vermelho acima dos 20%;
 */

function checksTheAverageOfTheGalcByState( $dataListFtth )
{

    $resultStates  = [];
    $arrQuantidade = [];
    $averageByDate = 0;

    foreach ( $dataListFtth as $key => $value ) {

        array_push( $arrQuantidade, $value[ 'TOTAL_7' ] );
        array_push( $arrQuantidade, $value[ 'TOTAL_14' ] );
        array_push( $arrQuantidade, $value[ 'TOTAL_21' ] );
        array_push( $arrQuantidade, $value[ 'TOTAL_28' ] );
        array_push( $arrQuantidade, $value[ 'TOTAL_35' ] );

        asort( $arrQuantidade );
        array_shift( $arrQuantidade );
        array_pop( $arrQuantidade );

        $somaTotal = array_sum( $arrQuantidade );

        $dataListFtth[ $key ][ 'media' ] = number_format( $somaTotal / 3, 2, ',', '.' );

        $dataListFtth[ $key ][ 'media_detachment' ] = (historicalMediaDetachment( $dataListFtth[ $key ] ) <= 0) ? 'OK': historicalMediaDetachment( $dataListFtth[ $key ] );

        $dataListFtth[ $key ][ 'media_detachment_sem_format' ] = prepareNumber($dataListFtth[ $key ][ 'media_detachment' ]);

        $dataListFtth[ $key ][ 'color_alarm' ] = checkAlarm( $dataListFtth[ $key ][ 'media_detachment' ], $dataListFtth[ $key ][ 'media' ], $value[ 'TOTAL' ] );

        $arrQuantidade = [];
    }

    return $dataListFtth;
}

/**
 * Deslocamento da média historica, falta definir quando a média for 0, momentaneamente vai ser 1 até que for descidido
 * pela usuário
 * mediaDetachment = ((TOTAL - MEDIA) / MEDIA ) * 100
 *
 * Comprovação:  MEDIA + mediaDetachment %  (o result comparar com o TOTAL e aplicar a regra de cor)
 * @param $dataMedia
 * @return string
 */
function historicalMediaDetachment( $dataMedia )
{
    //$media      = (float)str_replace( ',', '.', $dataMedia[ 'media' ] );
    $media       = (float)$dataMedia[ 'media' ];
    $total60min = (int)$dataMedia[ 'TOTAL' ];

    if ( $media == 0 ) {
        $mediaDetachment = 100;
    } else {
        $mediaDetachment = (float) (( ( $total60min - $media ) / $media ) * 100);
    }
    
    return  number_format($mediaDetachment, 2, ',', '.');
}


function checkAlarm( $averageDetachment, $average, $currentValue )
{

    $averageDetachment = prepareNumberComma($averageDetachment);
    $media             = (float)$average;
    $currentValue      = $currentValue;
    $valorPercentual   = 50; //Esse valor foi acordado por que estava retornado todo vermelho

//
//    $averageDetachment = (float)(prepareNumber('1,40000'));
//    $media             = (float)(prepareNumber("1,00"));
//    $currentValue      = (float)(prepareNumber("15"));
//    $valorPercentual   = (float)(prepareNumber("50"));


    if ( $averageDetachment >= 151 && $averageDetachment <= 500 ) {
        $colorAlarm = 'alarm-yellow';
    } elseif ( $averageDetachment >= 501 ) {
        $colorAlarm = 'alarm-red';
    } else {
        $colorAlarm = 'alarm-green';
    }

    return $colorAlarm;
}

/**
 * @param $connFTTH
 * @param $galc
 * Retorna os dados da porta PON na hora que foi clicado o GALC
 *
 * @return mixed
 */
function getQtdPortaPontByGalc( $connFTTH, $galc )
{
    $sql2 = "
        SELECT p.PORTA_PON, p.BA, p.GALC_OLT, p.UF, p.MUNICIPIO, p.LOCALIDADE, p.NUM_EVENTO,p.TIPO_EVENTO, 
        TO_CHAR(MAX(p.DT_HH_CONSULTA),'YYYY-MM-DD HH24:MI:SS') DT_HH_CONSULTA,
        COUNT(DISTINCT p.CLIENTE) QTD
        FROM PROCESSOS_FTTH p 
        WHERE p.GALC_OLT = '{$galc}'
        AND p.DT_HH_CONSULTA >= SYSDATE - 60/24/60 
        GROUP BY p.PORTA_PON, p.BA, p.GALC_OLT, p.UF, p.MUNICIPIO, p.LOCALIDADE, p.NUM_EVENTO,p.TIPO_EVENTO
        ORDER BY MAX(p.DT_HH_CONSULTA) DESC , QTD DESC
    ";
    
    
    $query = $connFTTH->prepare( $sql2 );

    $query->execute();

    $listPortaPont = $query->fetchAll( \PDO::FETCH_ASSOC );
    
    $connFTTH->close();

    return $listPortaPont;
}

function getQtdPortaPontByState( $connFTTH, $galc )
{
    $sql2 = "
        SELECT p.PORTA_PON, p.GALC_OLT, p.UF, p.MUNICIPIO, p.LOCALIDADE, p.NUM_EVENTO,p.TIPO_EVENTO, 
        TO_CHAR(MAX(p.DT_HH_CONSULTA),'YYYY-MM-DD HH24:MI:SS') AS DT_HH_CONSULTA,
        COUNT(DISTINCT p.CLIENTE) QTD
        FROM PROCESSOS_FTTH p 
        WHERE p.GALC_OLT = '{$galc}'
        AND p.DT_HH_CONSULTA >= SYSDATE - 60/24/60 
        GROUP BY p.PORTA_PON, p.GALC_OLT, p.UF, p.MUNICIPIO, p.LOCALIDADE, p.NUM_EVENTO,p.TIPO_EVENTO
        ORDER BY MAX(p.DT_HH_CONSULTA) DESC, QTD DESC
    ";
  
    $query = $connFTTH->prepare( $sql2 );

    $query->execute();

    $listPortaPont = $query->fetchAll( \PDO::FETCH_ASSOC );
  
    $connFTTH->close();

    return $listPortaPont;
}


function prepareNumber($number){
    $data =   str_replace('.', '', $number );
    $data =   str_replace(',', '', $data );
    return $data;
}

function prepareNumberComma($number){
//    $data =   str_replace(',', '', $number );
    $data =   str_replace('.', '', $number );
    return $data;
}
