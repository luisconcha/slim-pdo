<?php
/**
 * File: config.php
 * Author: Luis Alberto Concha Curay
 * Email: luvett11@gmail.com
 * Language: PHP
 * Date: 13/03/15
 * Time: 20:14
 * Project: slim
 * Copyright: 2015
 */

session_start();
ini_set( 'display_errors', 1 );

include 'config/Conection.php';
include 'config/ConectionFTTH.php';
include 'config/ConectionSBAO6002.php';

define( 'ROOT', $_SERVER[ 'DOCUMENT_ROOT' ] . DIRECTORY_SEPARATOR );
define( 'BASE_URL', $_SERVER[ 'HTTP_HOST' ] . DIRECTORY_SEPARATOR );

require ROOT . 'vendor/autoload.php';


$app = new \Slim\Slim();

$app->config( array(
    'debug'          => true,
    'templates.path' => 'views',
) );


$connGMON     = new \config\Conection();
$connFTTH     = new \config\ConectionFTTH();
//$connFTTH     = new \config\ConectionSBAO6002();
//$connSBAO6002 = new \config\ConectionSBAO6002();

