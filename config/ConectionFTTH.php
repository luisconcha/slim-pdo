<?php

namespace config;

class ConectionFTTH
{
    private static $instace = null;
    private static $server = "10.58.177.124";
    private static $db_username = "ftth";
    private static $db_password = "u5w9pqc6";
    private static $service_name = "opsistdb";
    private static $sid = "opsistdb";
    private static $port = 1521;

    public static function getInstance()
    {

        $dbtns = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = " . self::$server . ")(PORT = " . self::$port . ")) (CONNECT_DATA = (SID = " . self::$sid . ")))";

        self::$instace = new \PDOOCI\PDO( "oci:dbname=" . $dbtns . ";charset=utf8", self::$db_username, self::$db_password, array(
            \PDOOCI\PDO::ATTR_ERRMODE            => \PDOOCI\PDO::ERRMODE_EXCEPTION,
            \PDOOCI\PDO::ATTR_EMULATE_PREPARES   => false,
            \PDOOCI\PDO::ATTR_DEFAULT_FETCH_MODE => \PDOOCI\PDO::FETCH_ASSOC ) );

        return self::$instace;
    }

    public static function prepare( $sql )
    {
        return self::getInstance()->prepare( $sql );

    }

    public static function close()
    {
        if ( self::$instace != null ) {
            self::$instace = null;
        }
    }
}
