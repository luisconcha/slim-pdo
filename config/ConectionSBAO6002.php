<?php

namespace config;

class ConectionSBAO6002
{
    private static $instace = null;
    private static $server = "orapx03-scan.brasiltelecom.com.br";
    private static $db_username = "SBAO6002";
    private static $db_password = "$3rvr3f1re.p";
    private static $service_name = "spot";
    private static $sid = "spot";
    private static $port = 1549;

//    private static $instace = null;
//    private static $server = "localhost";
//    private static $db_username = "hr";
//    private static $db_password = "password";
//    private static $service_name = null;
//    private static $sid = "xe";
//    private static $port = 1521;

    public static function getInstance()
    {

        $dbtns = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = " . self::$server . ")(PORT = " . self::$port . ")) (CONNECT_DATA = (SERVICE_NAME = " . self::$service_name . ") (SID = " . self::$sid . ")))";

        self::$instace = new \PDOOCI\PDO( "oci:dbname=" . $dbtns . ";charset=utf8", self::$db_username, self::$db_password, array(
            \PDOOCI\PDO::ATTR_ERRMODE            => \PDOOCI\PDO::ERRMODE_EXCEPTION,
            \PDOOCI\PDO::ATTR_EMULATE_PREPARES   => false,
            \PDOOCI\PDO::ATTR_DEFAULT_FETCH_MODE => \PDOOCI\PDO::FETCH_ASSOC ) );

        return self::$instace;
    }

    public static function prepare( $sql )
    {
        return self::getInstance()->prepare( $sql );

    }

    public static function close()
    {
        if ( self::$instace != null ) {
            self::$instace = null;
        }
    }
}
