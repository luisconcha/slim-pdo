<?php

namespace config;

class Conection
{
    private static $instace = null;

//SBAO6002
//    private static $server = "orapx03-scan.brasiltelecom.com.br";
//    private static $db_username = "SBAO6002";
//    private static $db_password = "$3rvr3f1re.p";
//    private static $service_name = "spot";
//    private static $sid = "spot";
//    private static $port = 1549;

//GCORDDB
    private static $server = "10.121.1.207";
    private static $db_username = "GMON";
    private static $db_password = "gmon2019";
    private static $service_name = "GCORDB";
    private static $sid = "GCORDB";
    private static $port = 1521;

    public static function getInstanceGMON()
    {

        $dbtns = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = " . self::$server . ")(PORT = " . self::$port . ")) (CONNECT_DATA = (SERVICE_NAME = " . self::$service_name . ") (SID = " . self::$sid . ")))";

        self::$instace = new \PDOOCI\PDO( "oci:dbname=" . $dbtns . ";charset=utf8", self::$db_username, self::$db_password, array(
            \PDOOCI\PDO::ATTR_ERRMODE            => \PDOOCI\PDO::ERRMODE_EXCEPTION,
            \PDOOCI\PDO::ATTR_EMULATE_PREPARES   => false,
            \PDOOCI\PDO::ATTR_DEFAULT_FETCH_MODE => \PDOOCI\PDO::FETCH_ASSOC ) );

        return self::$instace;
    }

    public static function prepare( $sql )
    {
        return self::getInstanceGMON()->prepare( $sql );

    }

    public static function close()
    {
        if ( self::$instace != null ) {
            self::$instace = null;
        }
    }
}
