<?php
/**
 * File: helpers.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 13/06/19
 * Time: 10:09
 * Project: slim_api
 * Copyright: 2019
 */

if ( !function_exists( 'dateHoraMinuteBR' ) ) {
    function dateHoraMinuteBR( $date )
    {
        $objDate    = new DateTime( $date );
        $dateFormat = $objDate->format( 'd/m/Y H:i:s' );
        
        return $dateFormat;
    }
}

