<?php
/**
 * File: JsonResponse.php
 * Created by: Luis Alberto Concha Curay.
 * Email: luisconchacuray@gmail.com
 * Language: PHP
 * Date: 17/04/19
 * Time: 17:09
 * Project: global_maps
 * Copyright: 2019
 */


/**
 * Function to prepare return data in array to json
 * @param $app
 * @param $dataArray
 *
 * @return mixed
 */
function prepareJsonResponse( $app, $dataArray )
{
    $response                   = $app->response();
    $response[ 'Content-Type' ] = 'application/json';
    $response->status( 200 );

    return $response->body( json_encode( $dataArray ) );
}
